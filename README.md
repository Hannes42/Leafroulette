# Leafroulette
A screensaver, CPU burner, background animation thing build with the 
mapping library Leaflet.

https://hannes42.gitlab.io/Leafroulette/

Add layers, specify extents and have it fly and pan around in it, 
blending smoothly between layers every now and then.

![](https://gitlab.com/Hannes42/Leafroulette/raw/master/leafroulette.png)
https://giphy.com/gifs/l2RgjWneYlIeQwlxbL

It's tailored to what I needed, probably written very inefficiently but 
it does exactly what I wanted to achieve.

Since this is quite a weird hack and Leaflet does not really like 
to be scrolled anyways, this might be jiggly, broken and if you
do not have 2D acceleration, eat your CPU. I had no luck running
it smoothly on a Raspberry Pi 3...

Use local tiles or at least a cache if you plan to have this running
for more than a few minutes.

Best viewed in fullscreen and with only the UI elements you want.

**Don't ever click into the map, it will unsync the maps.**

## License
I declare the index.html CC-0, take whatever you need and have fun!

## How does it work?
Because I could not find a way to smoothly blend between map layers in
Leaflet, this uses two seperate, independent maps and blends them via
CSS instead. YOLO.

## Keys
- f to make the view fly somewhere
- p to make the view pan somewhere
- r to change the currently invisible layer randomly
- s to fade to the other current layer
- m to toggle the minimap

You can enable some buttons for these, see the CSS rules.
